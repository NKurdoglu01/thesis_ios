//
//  Session.swift
//  Thesis
//
//  Created by Naci Kurdoglu on 17/02/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import Foundation
import Firebase

class Session {
    
    private(set) static var authState:Auth?
    private(set) static var authUser:User?
    
    private static var loginStateChangeHandlers:[()->()] = []
    private static var loginStateListener:AuthStateDidChangeListenerHandle {
        return Auth.auth().addStateDidChangeListener { (auth, user) in
            Session.authState = auth
            for handler in Session.loginStateChangeHandlers {
                handler()
            }
            
        }
    }
    
    public static func configure(){
        
    }
    
    public static func addLoginStateChangeHandler(_ handler:@escaping ()->()){
        Session.loginStateChangeHandlers.append(handler)
    }
    
}
