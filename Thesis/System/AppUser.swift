//
//  User.swift
//  Thesis
//
//  Created by Naci Kurdoglu on 17/02/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import Foundation
import Firebase

class AppUser {
    
    public static var user:User?
    public static var name:String = ""
    public static var surname:String = ""
    public static var discountsEnabled:Bool = true
    public static var newProductsEnabled:Bool = true

    
    public static func initUser() {
        Database.database().reference().child("users/\(AppUser.user!.uid)").observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String:AnyObject] {
                AppUser.name = (dict["name"] as! String)
                AppUser.surname = (dict["surname"] as! String)
                AppUser.discountsEnabled = (dict["discounts"] as! Bool)
                AppUser.newProductsEnabled = (dict["newProducts"] as! Bool)
            }
        }
    }
    
    public static var isAutoLoginEnabled:Bool {
        return UserDefaults.standard.bool(forKey: "isAutoLoginEnabled")
    }
    
    public static var email:String {
        return UserDefaults.standard.string(forKey: "mainUserEmail") ?? ""
    }
    
    public static var password:String {
        return UserDefaults.standard.string(forKey: "mainUserPassword") ?? ""
    }
    
    public static func enableAutoLogin(){
        UserDefaults.standard.set(true, forKey: "isAutoLoginEnabled")
    }
    
    public static func setEmail(_ email:String){
        UserDefaults.standard.set(email, forKey: "mainUserEmail")
    }
    public static func setPassword(_ password:String){
        UserDefaults.standard.set(password, forKey: "mainUserPassword")
    }
}
