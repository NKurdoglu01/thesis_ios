//
//  Store.swift
//  Thesis
//
//  Created by Naci Kurdoglu on 18/02/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import Foundation
import Firebase
import GeoFire
import SwiftyJSON

var requestCount = 0

struct Store {
    private(set) static var stores:[String:Store] = [:]
    let storeId:String
    let name:String
    let promotions:[Promotion]
    var address:Address
    
    public static func initStores(location:CLLocation , radius:Double,_ success:@escaping (Store)->()){
        let ref = Database.database().reference()

        let start = Date().timeIntervalSince1970
        GeoFire(firebaseRef: ref.child("locations")).query(at: location, withRadius: radius).observe(.keyEntered) { (key, location) in
            
            ref.child("stores/\(key)").observeSingleEvent(of: .value) { (snapshot) in
                if let dict = snapshot.value as? Dictionary<String,AnyObject> {
                    
                    print("request " + requestCount.description + " time: " + ((Date().timeIntervalSince1970) - start).description)
                    requestCount+=1
                    
                    var store = Store.parseStoreData(storeInfo: JSON(dict))
                    store.address.location = location
                    Store.stores[key] = store
                    success(store)
                }
            }
        }
    }
    
    private static func parseStoreData(storeInfo:JSON) -> Store {
        
        let name = storeInfo["name"].stringValue
        let address = storeInfo["address"]
        
        let country = address["country"].stringValue
        let city = address["city"].stringValue
        let street = address["street"].stringValue
        let area = address["area"].stringValue
        let apartmentNo = address["apartmentNo"].intValue
        
        var promotionsArray:[Promotion] = []
        let promotions = storeInfo["promotions"]
        for promotionBase in promotions {
            
            let promotionId = promotionBase.0
            let promotion = promotionBase.1
            
            let prmotionType = promotion["type"].intValue
            if (prmotionType == 0 && AppUser.discountsEnabled) || (prmotionType == 1 && AppUser.newProductsEnabled) {
                let promotionDescription = promotion["description"].stringValue
                let promotionExpirationDate = promotion["expires"].doubleValue
                let date = Date(timeIntervalSince1970: promotionExpirationDate)
                
                let promotionStruct = Promotion(id: promotionId, storeName: name, description: promotionDescription, expirationDate: date)
                promotionsArray.append(promotionStruct)
            }
        }
        
        let addressDescriptionStruct = AddressDescription(apartmentNo: apartmentNo, area: area, city: city, country: country, street: street)
        
        let addressStruct = Address(location: nil, description: addressDescriptionStruct)
        let storeStruct = Store(storeId: storeInfo.dictionaryValue.keys.first!, name: name, promotions: promotionsArray, address: addressStruct)
        
        return storeStruct
    }
}

struct Address {
    var location:CLLocation?
    let description:AddressDescription
}

struct AddressDescription {
    let apartmentNo:Int
    let area:String
    let city:String
    let country:String
    let street:String
}

struct Promotion {
    let id:String
    let storeName:String
    let description:String
    let expirationDate:Date
}
