//
//  ViewController.swift
//  Thesis
//
//  Created by Naci Kurdoglu on 17/02/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase

class OpeningViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if AppUser.isAutoLoginEnabled {
            
            Auth.auth().signIn(withEmail: AppUser.email, password: AppUser.password) {authDataResult, error in
                if let err = error{
                    print(err)
                    
                    let loginVC = StoryboardScene.Login.initialScene.instantiate()
                    self.present(loginVC, animated: true, completion: nil)
                }else{
                    AppUser.user = authDataResult?.user
                    AppUser.enableAutoLogin()
                    AppUser.initUser()
                    
                    let mainTabBarVC = StoryboardScene.MainTabBar.initialScene.instantiate()
                    self.present(mainTabBarVC, animated: true, completion: nil)
                }
            }
        }else{
            let loginVC = StoryboardScene.Login.initialScene.instantiate()
            self.present(loginVC, animated: true, completion: nil)
        }
    }
}

