//
//  SettingsViewController.swift
//  Thesis
//
//  Created by Naci Kurdoglu on 18/02/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase
import SCLAlertView


class SettingsViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var surnameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var discountsSwitch: UISwitch!
    @IBOutlet weak var newProductsSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.emailField.text = AppUser.email
        self.nameField.text = AppUser.name
        self.surnameField.text = AppUser.surname
        self.discountsSwitch.isOn = AppUser.discountsEnabled
        self.newProductsSwitch.isOn = AppUser.newProductsEnabled
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func updateAction(_ sender: Any) {
        
        if self.nameField.text!.isEmpty{
            
            let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
            let alertView = SCLAlertView(appearance: appearance)
            let txt = alertView.addTextField("Enter your name")
            alertView.addButton("Update") {
                self.nameField.text = txt.text
                self.updateAction(sender)
            }
            
            alertView.showWarning("Enter your name", subTitle: "Please enter your name")
            
        }else if self.surnameField.text!.isEmpty {
            let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
            let alertView = SCLAlertView(appearance: appearance)
            let txt = alertView.addTextField("Enter your surname")
            alertView.addButton("Update") {
                self.surnameField.text = txt.text
                self.updateAction(sender)
            }
            
            alertView.showWarning("Enter your surname", subTitle: "Please enter your surname")
            
        }else if self.emailField.text!.isEmpty {
            let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
            let alertView = SCLAlertView(appearance: appearance)
            let txt = alertView.addTextField("Enter your e-mail")
            alertView.addButton("Update") {
                self.emailField.text = txt.text
                self.updateAction(sender)
            }
            
            alertView.showWarning("Enter your e-mail", subTitle: "Please enter your e-mail")
            
        }else{
        
            Database.database().reference().child("users/\(AppUser.user!.uid)").updateChildValues([
                "name":self.nameField.text!,
                "surname":self.surnameField.text!,
                "discounts":self.discountsSwitch.isOn,
                "newProducts":self.newProductsSwitch.isOn])
        }
    }
    
    @objc private func hideKeyboard(){
        view.endEditing(true)
    }
    
}
