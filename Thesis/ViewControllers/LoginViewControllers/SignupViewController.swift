//
//  SignupViewController.swift
//  Thesis
//
//  Created by Naci Kurdoglu on 17/02/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase
import SCLAlertView

class SignupViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var surnameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var repeatPasswordField: UITextField!
    @IBOutlet weak var discountsSwitch: UISwitch!
    @IBOutlet weak var newProductsSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }

    @IBAction func signupAction(_ sender: Any) {
     
        let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
        let alertView = SCLAlertView(appearance: appearance)
        
        if self.nameField.text!.isEmpty{
            
            let txt = alertView.addTextField("Enter your name")
            alertView.addButton("Sign up") {
                self.nameField.text = txt.text
                self.signupAction(sender)
            }
            
            alertView.showWarning("Enter your name", subTitle: "Please enter your name")

        }else if self.surnameField.text!.isEmpty {
            let txt = alertView.addTextField("Enter your surname")
            alertView.addButton("Sign up") {
                self.surnameField.text = txt.text
                self.signupAction(sender)
            }
            
            alertView.showWarning("Enter your surname", subTitle: "Please enter your surname")
            
        }else if self.emailField.text!.isEmpty {
            let txt = alertView.addTextField("Enter your e-mail")
            alertView.addButton("Sign up") {
                self.emailField.text = txt.text
                self.signupAction(sender)
            }
            
            alertView.showWarning("Enter your e-mail", subTitle: "Please enter your e-mail")
            
        }else if self.passwordField.text!.isEmpty {
            let txt = alertView.addTextField("Create password")
            alertView.addButton("Sign up") {
                self.passwordField.text = txt.text
                self.signupAction(sender)
            }
            
            alertView.showWarning("Create password", subTitle: "Create a password for your account")
            
        }else if self.repeatPasswordField.text!.isEmpty {
            let txt = alertView.addTextField("Verify password")
            alertView.addButton("Sign up") {
                self.repeatPasswordField.text = txt.text
                self.signupAction(sender)
            }
            
            alertView.showWarning("Verify password", subTitle: "Repeat your password to verify")
            
        }else if self.passwordField.text != self.repeatPasswordField.text {
            let alertView = SCLAlertView()
            alertView.showWarning("Password verification fail", subTitle: "Password you entered in the verification field is different than your password")
            
        }else{
            Auth.auth().createUser(withEmail: self.emailField.text!, password: self.passwordField.text!) { authResult, error in
                
                if let err = error {
                    print(err)
                    let appearance = SCLAlertView.SCLAppearance(showCloseButton: false)
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Dismiss") {
                        alertView.dismiss(animated: true, completion: nil)
                    }
                    
                    alertView.showError("Error", subTitle: err.localizedDescription)
                    
                }else{
                    AppUser.user = authResult?.user
                    
                    Database.database().reference().child("users").setValue(
                        [AppUser.user!.uid
                            :["name":self.nameField.text!,
                              "surname":self.surnameField.text!,
                              "discounts":self.discountsSwitch.isOn,
                              "newProducts":self.newProductsSwitch.isOn]
                        ])

                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        
    }
    
    @objc private func hideKeyboard(){
        view.endEditing(true)
    }
    
    
}
