//
//  LoginViewController.swift
//  Thesis
//
//  Created by Naci Kurdoglu on 17/02/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func hideKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        Auth.auth().signIn(withEmail: self.emailField.text!, password: self.passwordField.text!) {authDataResult, error in
            if let err = error{
                print(err)
            }else{
                AppUser.setEmail(self.emailField.text!)
                AppUser.setPassword(self.passwordField.text!)
                AppUser.enableAutoLogin()
                
                AppUser.user = authDataResult?.user
                AppUser.initUser()
                
                let mainTabBarVC = StoryboardScene.MainTabBar.initialScene.instantiate()
                self.present(mainTabBarVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func signupAction(_ sender: Any) {
        let signupVC = StoryboardScene.Login.signupViewController.instantiate()
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
    @IBAction func resetPasswordAction(_ sender: Any) {
    }
    
}
