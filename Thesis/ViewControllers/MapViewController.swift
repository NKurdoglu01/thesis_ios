//
//  MapViewController.swift
//  Thesis
//
//  Created by Naci Kurdoglu on 18/02/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    private let locationManager = CLLocationManager()
    private var updatedLocation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        VersionControl.checkVersion()
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        
        self.mapView.showsUserLocation = true;
        self.mapView.setCenter(self.mapView.userLocation.coordinate, animated: true)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if !self.updatedLocation {

            self.setStoreListener(location: locations.last!)
            
            self.mapView.setCenter(locations.last!.coordinate, animated: true)
            let viewRegion = MKCoordinateRegion(center: locations.last!.coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
            self.mapView.setRegion(viewRegion, animated: false)
            
            self.updatedLocation = true
        }
    }
    
    private func setStoreListener(location:CLLocation){

        Store.initStores(location: location, radius: 4) { (store) in
            let annotation = MKPointAnnotation()
            annotation.coordinate = store.address.location!.coordinate
            annotation.title = store.name
            self.mapView.addAnnotation(annotation)
        }
    }
    
}
