//
//  PromotionTableViewCell.swift
//  Thesis
//
//  Created by Naci Kurdoglu on 18/02/2019.
//  Copyright © 2019 NK. All rights reserved.
//

import UIKit

class PromotionTableViewCell: UITableViewCell {

    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var expirationDateLabel: UILabel!
    
    private var promotion:Promotion!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    public func setPromotion(_ promotion:Promotion){
        self.promotion = promotion
        
        self.storeName.text = self.promotion.storeName
        self.descriptionLabel.text = self.promotion.description
        self.expirationDateLabel.text = self.promotion.expirationDate.description
    }

}
